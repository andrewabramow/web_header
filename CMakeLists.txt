﻿# CMakeList.txt : CMake project for example2, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project ("web_header")

set(CMAKE_CXX_STANDART 14)
set(BUILD_CPR_TESTS OFF)
set(CMAKE_USE_OPENSSL OFF)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

include(FetchContent)
FetchContent_Declare(cpr GIT_REPOSITORY https://github.com/whoshuu/cpr.git GIT_TAG c8d33915dbd88ad6c92b258869b03aba06587ff9) 
# the commit hash for 1.5.0
FetchContent_MakeAvailable(cpr)

# Add source to this project's executable.
add_executable (web_header "web_header.cpp" "web_header.h")
target_link_libraries(web_header PRIVATE cpr::cpr)

# TODO: Add tests and install targets if needed.
