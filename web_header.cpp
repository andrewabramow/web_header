﻿/*
Запросите с сервера HTML-страницу. Для этого изучите
документацию о запросах, поддерживаемых сервисом
httpbin.org. Найдите там запрос, который выдаёт именно
HTML-страницу. Затем сделайте запрос, явным образом
используя заголовок “accept: text/html”. В полученном
ответе найдите заголовок статьи и выведите его в консоль.
*/
#include <iostream>
#include <iterator>
#include <cpr/cpr.h>
#include "web_header.h"

int main()
{
	cpr::Response r = cpr::Get(cpr::Url("http://httpbin.org/html"),
		cpr::Header({ {"Accept", "text/html"} }));

	// finding header
	int start = 0;
	int end = 0;
	for (int i = 0; i < r.text.size(); ++i) {
		if (r.text[i] == 'h' && r.text[i + 1] == '1') {
			if (start == 0) {
				start = i;
			}
			else {
				end = i;
				break;
			}
		}
	}
	std::string s = r.text.substr(start + 3, (end - 2)-(start + 3));
	std::cout << s << std::endl;
}
